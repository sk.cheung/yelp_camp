var mongoose   = require("mongoose"),
    Campground = require("./models/campground"),
    Comment    = require("./models/comment");

var data = [{
        name: "Cloud's Rest",
        image: "https://farm9.staticflickr.com/8161/7360193870_cc7945dfea.jpg",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam justo nisl, porttitor et vehicula at, efficitur a sem. Vivamus commodo vehicula gravida. Sed id faucibus nunc. Curabitur mollis feugiat velit ac porttitor. Morbi non vestibulum tortor. Praesent vulputate tellus est, iaculis tristique orci porttitor vitae. Aenean non turpis sed urna accumsan feugiat vitae ut leo."
    },
    {
        name: "Desert Mesa",
        image: "https://farm4.staticflickr.com/3872/14435096036_39db8f04bc.jpg",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam justo nisl, porttitor et vehicula at, efficitur a sem. Vivamus commodo vehicula gravida. Sed id faucibus nunc. Curabitur mollis feugiat velit ac porttitor. Morbi non vestibulum tortor. Praesent vulputate tellus est, iaculis tristique orci porttitor vitae. Aenean non turpis sed urna accumsan feugiat vitae ut leo."
    },
    {
        name: "Canyon Floor",
        image: "https://farm2.staticflickr.com/1363/1342367857_2fd12531e7.jpg",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam justo nisl, porttitor et vehicula at, efficitur a sem. Vivamus commodo vehicula gravida. Sed id faucibus nunc. Curabitur mollis feugiat velit ac porttitor. Morbi non vestibulum tortor. Praesent vulputate tellus est, iaculis tristique orci porttitor vitae. Aenean non turpis sed urna accumsan feugiat vitae ut leo."
    }
];

function seedDB() {
    // Remove all campgrounds
    Campground.remove({}, function(err) {
        if (err) {
            console.log(err);
        }
        console.log("removed campgrounds!");
        // Add a few campgrounds
        data.forEach(function(seed) {
            Campground.create(seed, function(err, campground) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("Added new campgrounds");
                    // Create a comment
                    Comment.create({
                        text: "This place is great but I wish there was Internet",
                        author: "Homer"
                    }, function(err, comment) {
                        if (err) {
                            console.log(err);
                        } else {
                            campground.comments.push(comment);
                            campground.save();
                            console.log("Created new comment");
                        }
                    });
                }
            });
        });
    });
}

module.exports = seedDB;
